#+title: My Config Personal
#+author: Osvarcha

Lexical Bindings

#+begin_src emacs-lisp :tangle yes
;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-
#+end_src

* Config General
** Datos Generales

Información general

#+begin_src emacs-lisp
(setq user-full-name "Oscar (osvarcha) Vargas"
      user-mail-address "osvarcha@hotmail.com")
#+end_src

** Config General Emacs

#+begin_src emacs-lisp
;; Para mostar enumeración lineal
(setq display-line-numbers-type 'relative)
;; Para la columna margen
(add-hook 'prog-mode-hook #'display-fill-column-indicator-mode)
(add-hook 'org-mode-hook #'display-fill-column-indicator-mode)
;; La fuente en FiraCode
(setq do-font "FantasqueSansMono-12")
;; Tema de doom
(setq doom-theme 'doom-dracula)
#+end_src

* Emacs Applications Framework

Lo que que dara potencia a Emacs es EAF, un browser, lector y demas.

#+begin_src emacs-lisp
(use-package! eaf
  :load-path "~/.elisp/emacs-application-framework"
  :init
  :custom
  (eaf-browser-continue-where-left-off t)
  (eaf-browser-enable-adblocker t)
  (browse-url-browser-function 'eaf-open-browser)
  :config
  (defalias 'browse-web #'eaf-open-browser)
  (setq eaf-browser-translate-language "es")
  (require 'eaf-browser)
  (require 'eaf-terminal)
  (require 'eaf-all-the-icons)
  (require 'eaf-music-player)
  (require 'eaf-org)
  (require 'eaf-org-previewer)
  (require 'eaf-video-player)
  (require 'eaf-image-viewer)
  (require 'eaf-terminal)
  (require 'eaf-camera)
  (require 'eaf-markdown-previewer)
  (require 'eaf-org-previewer)
  (require 'eaf-git)
  (require 'eaf-file-manager)
  (require 'eaf-jupyter)
  (require 'eaf-system-monitor)
  ;; pdf
  (require 'eaf-pdf-viewer)
  (setq eaf-pdf-dark-mode nil)
  (setq eaf-org-dark-mode "ignore")

  ;; Evil
  (require 'eaf-evil)
  (define-key key-translation-map (kbd "SPC")
    (lambda (prompt)
      (if (derived-mode-p 'eaf-mode)
          (pcase eaf--buffer-app-name
            ("browser" (if  (string= (eaf-call-sync "call_function" eaf--buffer-id "is_focus") "True")
                           (kbd "SPC")
                         (kbd eaf-evil-leader-key)))
            ("pdf-viewer" (kbd eaf-evil-leader-key))
            ("image-viewer" (kbd eaf-evil-leader-key))
            (_  (kbd "SPC")))
        (kbd "SPC"))))
  )
#+end_src

* Tools
** Vundo

Un gestor de retroceso

#+begin_src emacs-lisp
(use-package vundo
  :defer t
  :config
  ;; Take less on-screen space.
  (setq vundo-compact-display t)

  ;; Better contrasting highlight.
  (custom-set-faces
    '(vundo-node ((t (:foreground "#808080"))))
    '(vundo-stem ((t (:foreground "#808080"))))
    '(vundo-highlight ((t (:foreground "#FFFF00")))))

  ;; Use `HJKL` VIM-like motion, also Home/End to jump around.
  (define-key vundo-mode-map (kbd "l") #'vundo-forward)
  (define-key vundo-mode-map (kbd "<right>") #'vundo-forward)
  (define-key vundo-mode-map (kbd "h") #'vundo-backward)
  (define-key vundo-mode-map (kbd "<left>") #'vundo-backward)
  (define-key vundo-mode-map (kbd "j") #'vundo-next)
  (define-key vundo-mode-map (kbd "<down>") #'vundo-next)
  (define-key vundo-mode-map (kbd "k") #'vundo-previous)
  (define-key vundo-mode-map (kbd "<up>") #'vundo-previous)
  (define-key vundo-mode-map (kbd "<home>") #'vundo-stem-root)
  (define-key vundo-mode-map (kbd "<end>") #'vundo-stem-end)
  (define-key vundo-mode-map (kbd "q") #'vundo-quit)
  (define-key vundo-mode-map (kbd "C-g") #'vundo-quit)
  (define-key vundo-mode-map (kbd "RET") #'vundo-confirm))

(with-eval-after-load 'evil (evil-define-key 'normal 'global (kbd "C-M-u") 'vundo))
#+end_src

* Text Edit
** Company

#+begin_src emacs-lisp
(use-package company
  :custom
  (company-idle-delay 1.5))
#+end_src
